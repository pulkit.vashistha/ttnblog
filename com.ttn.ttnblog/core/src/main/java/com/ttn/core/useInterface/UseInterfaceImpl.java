package com.ttn.core.useInterface;

import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.jws.soap.SOAPBinding;
import javax.script.Bindings;

public class UseInterfaceImpl implements Use {

    String name = "";
    @Override
    public void init(Bindings bindings) {
        Resource resource = (Resource)bindings.get("resource");
        ValueMap properties = (ValueMap)bindings.get("properties");

        // Parameters passed to the use-class
        String param1 = (String) bindings.get("param1");
       name = ""+properties.get(JcrConstants.JCR_PRIMARYTYPE).toString();

    }

    public String getSample(){
        return "favourite string";
    }

    public String getName(){
        return name;
    }
}
