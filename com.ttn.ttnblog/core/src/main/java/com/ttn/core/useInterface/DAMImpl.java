package com.ttn.core.useInterface;

import com.adobe.cq.sightly.WCMUsePojo;
import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.Rendition;
import org.apache.sling.api.resource.Resource;

public class DAMImpl extends WCMUsePojo {

    private String rendition;
    private String image;

    @Override
    public void activate() throws Exception {
        rendition = getProperties().get("rendition").toString();
        image = getProperties().get("image").toString();
    }

    public String getRendition() {
            return rendition;
    }

    public String getImage(){
        Resource resource = getResourceResolver().getResource(image);
        Asset asset = resource.adaptTo(Asset.class);
        try{

            Rendition rendition=asset.getRendition("cq5dam.thumbnail."+getRendition()+".png");
            return rendition.getPath();

        } catch (Exception ex) {

            Rendition rendition=asset.getRendition("original");
            return rendition.getPath();

        }

    }
}
