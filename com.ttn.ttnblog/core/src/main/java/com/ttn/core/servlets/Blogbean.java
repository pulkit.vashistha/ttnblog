package com.ttn.core.servlets;

import java.util.Date;

public class Blogbean implements Comparable{
     private Date blogDate;
     private String blogTitle;

    public Date getBlogDate() {
        return blogDate;
    }

    public void setBlogDate(Date blogDate) {
        this.blogDate = blogDate;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    @Override
    public int compareTo(Object o) {
        Blogbean blogbean = (Blogbean)o;
        return this.getBlogDate().compareTo(blogbean.getBlogDate());
    }
}
