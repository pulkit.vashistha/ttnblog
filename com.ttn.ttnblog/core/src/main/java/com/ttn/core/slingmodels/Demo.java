package com.ttn.core.slingmodels;

public interface Demo {
    String getAltText();
    String getLinkText();
    String getImagePath();
    String getLinkPath();
}
