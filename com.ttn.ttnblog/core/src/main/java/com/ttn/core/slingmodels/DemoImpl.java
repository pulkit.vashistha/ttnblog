package com.ttn.core.slingmodels;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class,
        adapters = Demo.class,
        resourceType = DemoImpl.RESOURCE_TYPE)
public class DemoImpl implements Demo{
    protected static final String RESOURCE_TYPE = "ttnblog/components/content/demo";

    @ValueMapValue(name = "altText")
    private String altText;

    @ValueMapValue(name = "linkText")
    private String linkText;

    @ValueMapValue(name = "linkPath")
    private String linkPath;

    @ValueMapValue(name = "imagePath")
    private String imagePath;


    @Override
    public String getAltText() {
        return altText;
    }

    @Override
    public String getLinkText() {
        return linkText;
    }

    @Override
    public String getImagePath() {
        return imagePath;
    }

    @Override
    public String getLinkPath() {
        return linkPath;
    }
}
