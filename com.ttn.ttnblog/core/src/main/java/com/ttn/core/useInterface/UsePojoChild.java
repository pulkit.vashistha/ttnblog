package com.ttn.core.useInterface;

import com.adobe.cq.sightly.WCMUsePojo;

public class UsePojoChild extends WCMUsePojo {
    private String name;
    @Override
    public void activate() throws Exception {
        name = get("name",String.class);
    }

    public String getName(){
        return name;
    }
}
