package com.ttn.core.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Insights {

//    public List<Insight> insightList;

//    @ChildResource
//    @Named("list")
//    public Resource list;

    @Inject
    @Named("list")
    private List<Insight> list;

//    @PostConstruct
//    protected void init() {
//        insightList = new ArrayList<>();
//        if(insightList != null) {
//            logger.info("value of header list is "+list);
//            insightList = populateModel(insightList,list);
//        }
//    }

//    public List<Insight> populateModel(List<Insight> array, Resource resource) {
//        if (resource != null) {
//            Iterator<Resource> linkResource = resource.listChildren();
//            while (linkResource.hasNext()) {
//                Insight insight = linkResource.next().adaptTo(Insight.class);
//                if (insight != null) {
//                    array.add(insight);
//                }
//            }
//        }
//        return array;
//    }

    public List<Insight> getLinks() {
        return list;
    }


}
