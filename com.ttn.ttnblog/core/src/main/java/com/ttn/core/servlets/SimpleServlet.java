/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.ttn.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.commons.util.DateParser;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type. The
 * {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are
 * idempotent. For write operations use the {@link SlingAllMethodsServlet}.
 */
@Component(service=Servlet.class,
           property={
                   Constants.SERVICE_DESCRIPTION + "=Simple Demo Servlet",
                   "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                   "sling.servlet.resourceTypes="+ "ttnblog/components/structure/mainpage",
//                   "sling.servlet.paths="+ "/bin/content/ttnblog/Mainpage",
                   "sling.servlet.extensions=" + "txt",
                   "sling.servlet.selectors=" + "abc"
           })
public class SimpleServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;
    private List<Blogbean> blogs = new ArrayList<>();

    @Override
    protected void doGet(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {

        final Resource resource = req.getResource();
        final List<Date> dateList = new ArrayList<>();
        for(Resource resource1 : resource.getChildren()){

            Blogbean blogbean = new Blogbean();
            blogbean.setBlogDate(DateParser.parseDate(resource1.getValueMap().get("jcr:created").toString()));
            blogbean.setBlogTitle(resource1.getValueMap().get("jcr:title").toString());
            blogs.add(blogbean);
        }

        Collections.sort(blogs);


        resp.setContentType("text/plain");
        resp.getWriter().write("This is Simple servlet");
        for(Blogbean blogbean:blogs)
        resp.getWriter().append("Title = " + blogbean.getBlogTitle() + "  \n ");


    }


}
